# Pycharm project setup

## Prerequisites

Using the provided project settings requires an installed version
of:

- [Docker Compose](https://docs.docker.com/compose/install/)
- [PyCharm Professional](https://www.jetbrains.com/pycharm/download/?section=linux)

Additionally, the virtual env setup for the File Watchers also requires:

- [Python 3.11](https://www.python.org/downloads/release/python-3115/)
- [Poetry](https://python-poetry.org/docs/#installation)

<br>

## Docker Setup for Pycharm

### Windows

PyCharm remote interpreter works well with Docker Desktop under Windows. You will need to install WSL first.

- [How to install Linux on Windows with WSL](https://learn.microsoft.com/en-us/windows/wsl/install)
- [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install/)

### Linux (Ubuntu)

As of this writing, PyCharm remote interpreter has issues connecting properly with Docker Desktop under linux.
It's advised to use Docker Engine instead:

- [Install using the Apt repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
- [Manage Docker as a non-root user](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)
- [Configure Docker to start on boot with systemd](https://docs.docker.com/engine/install/linux-postinstall/#configure-docker-to-start-on-boot-with-systemd)

#### Troubleshooting

:warning: **docker-compose command not working:**

- Create a docker-compose file: `sudo nano /usr/bin/docker-compose`
- Containing:

```bash
#!/usr/bin/env bash
docker compose $@
```

- Give permission to execute it:`sudo chmod a+rwx /usr/bin/docker-compose`

:warning: **[Cannot connect docker daemon](https://intellij-support.jetbrains.com/hc/en-us/community/posts/360010126039-Cannot-connect-to-the-Docker-daemon-at-unix-var-run-docker-sock-Is-the-docker-daemon-running-) error:**

- Run the following commands:

```bash
sudo chmod a+rwx /var/run/docker.sock
sudo chmod a+rwx /var/run/docker.pid
```

:warning: **[“docker-credential-desktop.exe”: executable not in $PATH](https://stackoverflow.com/questions/65896681/exec-docker-credential-desktop-exe-executable-file-not-found-in-path) error:**

- Edit the `"~/.docker/config.json"` file ,
- and delete the following JSON key/value: `"credsStore" : "desktop",`.

<br>

## Remote interpreter

Make sure the python interpreter is set to the Docker Compose configuration file provided with the project

<div align="center">
  <img src="remote_interpreter.png" alt="drawing" width="700" />
</div>

<br>

## Running tests in PyCharm

First, set up the external tools settings:

- Backup your tools settings:
    - _File_ >> _Manage IDE Settings_ >> _Export settings_
- Then import the settings for this project
    - _File_ >> _Manage IDE Settings_ >> _Import settings_ >> select `.run/pycharm_tools_settings.zip`

Afterwards run the **Linters & Tests** configuration (_Alt + Shift + F10_) .

<br>

## Running File Watchers

The project *file watchers* allow for automatic black and isort formatting.
To use them a virtual environment needs to be set up:

    python -m venv ./.venv
    poetry config virtualenvs.in-project true
    poetry install