<div align="center">
  <h1>Chain of Responsibility</h1>
</div>

<div align="center">
  <img src="chain_of_responsibility_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Chain of Responsibility is a behavioral pattern to pass a request along a chain of handlers
until one fulfills it.**

### Real-World Analogy

_Tech Support._

A caller is first connected to an autoresponder. If he cant solve the problem, you are transferred to a live operator.
When that doesn't solve the problem, you are passed on to an engineer.

### Participants

- :bust_in_silhouette:/:man: **Handler**
    - Stores nextHandler (aka successor).
    - Provides an implementation for:
        - `setNext` (aka `setSuccessor` or `then`)
    - Defines an abstract method for:
        - `handle` (aka `handleRequest`)
- :man: **ConcreteHandler**
    - Implements:
        - `handle` (aka `handleRequest`)

### Collaborations

When a client issues a request, the request propagates along the chain until a **ConcreteHandler** object takes
responsibility for handling it.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When the set of request handlers and their order are unknown, might change, or is determined
dynamically.**

### Motivation

- How can we replace a function containing a long list of if statements, that return when the condition is met?
- How can we dynamically change the set of possible handlers for a request?
- How can we dynamically change the order in which receivers get the chance to handle a request?
- How can we decouple the sender of a request from its receiver?

### Known Uses

- Handling domain events:
    - For example, in an e-commerce system, "OrderPlaced", "PaymentReceived", or "OrderShipped" events could each have
      its own set of handlers to perform tasks like sending confirmation emails, updating inventory, or triggering
      shipping procedures.
- Determining discount:
    - Apply discount if any of a number of conditions is met.
    - E.g: apply a discount if the customer is a VIP, or if the product is on the stock clearance list, or if the
      costumer has filled in a valid discount coupon code.
- Authentication:
    - E.g: first check if the username exists, then check if the password is valid, if the account is still active, if
      we received a valid 2FA code, and lastly determine if the user's role, group, department or organization allows
      access
- Multi-level sorting:
    - Sort on a characteristic, and on another if the first characteristic is equal.
    - for example sorting on birth century, and if 2 items are equal in that regard, sort on last name.
- GUI Event Handling:
    - In UI frameworks, where events like mouse clicks or keyboard input need to be processed by different handlers in a
      chain
    - E.g: capturing, bubbling mechanisms in DOM.
- Logging:
    - Log messages can be processed by multiple loggers, each responsible for a specific level of logging (info,
      warning, error) or for handling logs in different ways (file, console, database).
- Exception Handling:
    - When handling exceptions, a chain of exception handlers can attempt to deal with an exception at
      different levels of abstraction before it reaches a default handler.
- Input Validation:
    - In a form validation process where different validators handle specific types of input validation.
    - e.g., checking for required fields, validating email formats, etc.) in a chain.
- Resource Allocation:
    - a chain of handlers might determine which resource or pool of resources can fulfill a request.
- Middleware in Web Development:
    - In web frameworks like Express.js in Node.js, where a series of middleware functions can
      handle HTTP requests sequentially, each performing specific tasks like authentication, logging, or error handling.
- Caching Mechanisms:
    - multiple caches or layers of caches might be arranged in a chain to retrieve or store data. If the data is not
      found in one cache, the request can be passed to the next cache in the chain.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Behavioral patterns, in general, are concerned with the assignment of responsibilities between objects.
They’re not just patterns of objects, but also the patterns of communication between those objects.

Behavioral **object** patterns use composition to achieve cooperation between objects.
An important issue here is how these peer objects know about each other.

The Chain of Responsibility pattern manages how peers know each other by indirection, to allow for loose coupling.

### Aspects that can vary

- Object that can fulfill a request.

### Solution to causes of redesign

- Dependence on specific operations.
    - Directly using a particular operation commits the client to a single way of satisfying the request.
    - Avoiding hard-coded requests, make it easier to change the way a request gets satisfied.
- Tight coupling.
    - Hard to understand: a lot of context needs to be known to understand a part of the system.
    - Hard to change: changing one class necessitates changing many other classes.
    - Hard to reuse in isolation: because classes depend on each other.
- Extending functionality by subclassing.
    - Hard to understand: comprehending a subclass requires in-depth knowledge of all parent classes.
    - Hard to change: a modification to a parent might break multiple children classes.
    - Hard to reuse partially: all class members are inherited, even when not applicable, which can lead to a class
      explosion.

### Consequences

| Advantages                                                                                                                                                                                                           | Disadvantages                                                                                                                                                            |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Reduced coupling.** <br> Both the receiver and the sender of the request have no explicit knowledge of each other, and an object in the chain doesn't have to know about the chain's structure. | :x: **Requests may end up unhandled** <br> Since a request has no explicit receiver, there's no guarantee it'll be handled—the request can fall off the end of the chain |
| :heavy_check_mark: **Flexibly assign responsibilities.** <br> You can add or change responsibilities for handling a request by changing the chain at run-time.                                                       |                                                                                                                                                                          |
| :heavy_check_mark: **Flexibly define request handling order.** <br> It's easy to change the order with which handlers pick up the request by editing the chain.                                                      |                                                                                                                                                                          |

### Relations with Other Patterns

_Distinction from other patterns:_

- Command establishes unidirectional connections between senders and receivers.
- Mediator eliminates direct connections between senders and receivers, forcing them to communicate indirectly via a
  mediator object.
- Observer lets receivers dynamically subscribe to and unsubscribe from receiving requests.
- Chain of Responsibility and Decorator have very similar class structures. Both patterns rely on recursive composition
  to pass the execution through a series of objects. However, there are several crucial differences.
    - The Chain of Responsibility handlers can execute arbitrary operations independently of each other.
    - They can also stop passing the request further at any point.
    - Various Decorators can extend the object’s behavior while keeping it consistent with the base interface.
    - In addition, decorators aren’t allowed to break the flow of the request.

_Combination with other patterns:_

- Chain of Responsibility is often applied in conjunction with Composite. There, a component's parent can act as its
  successor.
- Handlers in Chain of Responsibility can be implemented as Commands. In this case, you can execute a lot of different
  operations over the same context object, represented by a request.
- In another approach the request itself is a Command object. In this case, you can execute the same operation in a
  series of different contexts linked into a chain.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **The Handler class, after executing its own handle method, calls handle on the stored nextHandler.**

### Structure

```mermaid
classDiagram
    class Handler {
        <<abstract>>
        - nextHandler: Handler
        + setNext(nextHandler: Handler): void
        + handle(request: Request)*
    }

    class ConcreteHandler {
        + handle(request: Request)
    }

    Handler <|.. ConcreteHandler: implements
    Handler o-- "nextHandler" Handler: aggregated by
```

### Variations

_Implementing the nextHandler chain:_

- **Define new links**: usually in the Handler, but ConcreteHandlers could define them instead.
    - :heavy_check_mark: Most flexible.
    - :x: Requires configuration.
- **Use existing links**: For example, the parent references in a composite object can be used.
    - :heavy_check_mark: saves you from defining links explicitly
    - :x: if the structure doesn't reflect the chain of responsibility you require, you'll need to create redundant
      links.

_Abstract handler or interface:_

- **As abstract class**: Handler maintains nextHandler, and provide a default implementation of handle method that
  forwards the request to the nextHandler (if any).
    - :heavy_check_mark: If a ConcreteHandler subclass isn't interested in the request, it doesn't have to override the
      forwarding operation.
    - :x: Introducing custom forwarding requires subclassing.
- **As interface**:
    - :heavy_check_mark: More flexibility for concrete handlers to set the next handler and provide forwarding
      operation.
    - :x: Might lead to code duplication / boilerplate code in concrete classes.

_Representing requests:_

For example, a GUI system that can display help about different elements, but also allows printing that help.

- **Hard-coded operation invocation**: method calls like: `handleHelp()`, `handlePrint()`
    - :heavy_check_mark: Convenient and safe.
    - :x: Can forward only the fixed set of requests that the Handler class defines.
- **Request code as parameter**: for example a string argument like `handle("help")`
    - :heavy_check_mark: More flexible.
    - :x: Requires conditional statements in the handle method.
    - :x: Not type safe.
- **Request object**: stores the type of request and is passed to the handle method: `handle(request)`
    - :heavy_check_mark: Type safe.
    - :x: Handlers get coupled to the request object interface.

_Continue or terminate_

What should happen when a handler successfully handles a request?

- **Continue processing the chain**
    - :heavy_check_mark:  Each handler can contribute its unique functionality to the final result.
    - :x: challenging to predict the final outcome in large and complex chains.
- **Terminate processing the chain**
    - :heavy_check_mark: predictable outcome, since it can only come from one handler
    - :x: If you want to introduce a new handler that should always execute regardless of previous handlers, it may
      require changes to the existing handlers or the overall structure of the chain.

### Implementation

In the example we apply the chain of responsibility pattern to an authentication system.

first check if the username exists, then check if the password is valid, if the account is still active, if
we received a valid 2FA code, and lastly determine if the user role permission for allowed access.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Chain of Responsibility](https://refactoring.guru/design-patterns/chain-of-responsibility)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ZoranHorvat: Chain of Responsibility to the Rescue!](https://youtu.be/i_dEHmWsZdA?si=tcuF4NCLgA5uOxPN)
- [Gui Ferreira: Refactoring C# with Chain of Responsibility](https://youtu.be/SF305UAQTXI?si=iLIpevYKnRdBqKOn)
- [Geekific: The Chain of Responsibility Pattern Explained & Implemented](https://youtu.be/FafNcoBvVQo?si=E8RN_GVdui0psHQh)

<br>
<br>
