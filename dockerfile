FROM python:3.11-slim

# pin installed poetry version
RUN pip install poetry==1.6.1

# install dependencies first for docker caching (only rebuild code changes)
WORKDIR /app
COPY pyproject.toml /app
COPY poetry.lock /app
RUN poetry config virtualenvs.create false
RUN poetry install

# copy the project code
COPY chain_of_responsibility /app/chain_of_responsibility
COPY tests /app/tests

# poetry install again for project (`poetry run` command)
RUN poetry install

CMD ["python -m chain_of_responsibility"]